/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package epg.view;

import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.EDIT_VIEW;
import static epg.StartupConstants.ICON_EDIT;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.ICON_VIEW;
import static epg.StartupConstants.LIVE_VIEW;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import epg.controller.ComponentController;
import epg.controller.FileController;
import epg.controller.PageEditController;
import epg.file.FileManager;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Ali
 */
public class PageUI {
    
    //Switchview toolbar
    HBox switchViewToolbarContainer;
    FlowPane switchViewToolbarPane;
    
    //Switchview toolbar buttons
    Button editViewButton;
    Button liveViewButton;
    
    //ADD Remove page toolbar
    VBox pageToolbarContainer;
    FlowPane pageToolbarPane;
    
    //ADD Remove page toolbar buttons
    Button addPageButton;
    Button removePageButton;
    
    //Component toolbar
    HBox addComponentToolbarContainer;
    FlowPane addComponentToolbarPane;
    
    //Component toolbar buttons
    Button addHeadingButton;
    Button addParagraphButton;
    Button addListButton;
    Button addImageeButton;
    Button addVideoButton;
    Button removeSlideshowButton;
    
    //ADD and Remove and Edit component buttons
    Button editHeaderButton;
    Button editListButton;
    Button editImageButton;
    Button editVideoButton;
    Button editSlideShowButton;
    Button editParagraphButton;
    
    Button removeHeaderButton;
    Button removeListButton;
    Button removeImageButton;
    Button removeVideoButton;
    Button removeSlideShowButton;
    Button removeParagraphButton;
    
    
    //COntroller
    FileController fileController;
    FileManager fileManager;
    PageEditController pageEditController;
    ComponentController componentController;
    
    //Content Area
    VBox contentArea;
    
    //Content
    VBox content;
    
    //Tab panes
    TabPane tabPane;
    
    //Paths
    Path html;
    
    //Containers
    HBox imageContainer;
    HBox videoContainer;
    HBox slideShowContainer;
    HBox paragraphContainer;
    HBox listContainer;
    HBox nameContainer;
    HBox titleContainer;
    HBox headerContainer;
    
    private MainUI ui;
    
    public PageUI(MainUI theui) throws MalformedURLException{
        ui = theui;
        initswitchViewToolbar();
        initContentArea();
        initEventHandlers();
    }
    
    public void initContentArea() throws MalformedURLException{
        contentArea = new VBox();
        content = new VBox();
        
        contentArea.setId("contentarea");
        TabPane switchPane = new TabPane();
        Tab editTab = new Tab("Edit");
        editTab.setContent(content);
        Tab liveTab = new Tab("Live");
        liveTab.setContent(createWebview());
        VBox.setVgrow(switchPane, Priority.ALWAYS);
        switchPane.getTabs().addAll(editTab,liveTab);
        contentArea.getChildren().add(switchPane);
        titleContainer = new HBox();
        Label titlePrompt = new Label("Title: ");
        TextField titleField = new TextField();
        titleContainer.getChildren().addAll(titlePrompt,titleField);
        content.getChildren().add(titleContainer);
        
        nameContainer = new HBox();
        Label namePrompt = new Label("Student Name: ");
        TextField nameField = new TextField();
        nameContainer.getChildren().addAll(namePrompt,nameField);
        content.getChildren().add(nameContainer);
        contentArea.getChildren().add(content);
        
        headerContainer = new HBox();
        Text header = new Text("hi");
        headerContainer.getChildren().add(header);
        editHeaderButton = initChildButton(headerContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeHeaderButton = initChildButton(headerContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        content.getChildren().add(headerContainer);
        
        Hyperlink link = new Hyperlink();
        link.setText("This is a hyperlink");
        link.setOnAction((e->{System.out.println("A link was clicked");}));
        
        paragraphContainer = new HBox();
        Text paragraph = new Text("hi");
        paragraphContainer.getChildren().add(paragraph);
        editParagraphButton = initChildButton(paragraphContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeParagraphButton = initChildButton(paragraphContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        content.getChildren().add(paragraphContainer);
        
        listContainer = new HBox();
        content.getChildren().add(listContainer);
        editListButton = initChildButton(listContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeListButton = initChildButton(listContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        imageContainer = new HBox();
        File file = new File("./images/img/w1.jpg");
        URL fileURL = file.toURI().toURL();
        ImageView imageView = new ImageView(new Image(fileURL.toExternalForm()));
        imageView.setFitWidth(600);
        imageView.setPreserveRatio(true);
        imageContainer.getChildren().add(imageView);
        editImageButton = initChildButton(imageContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeImageButton = initChildButton(imageContainer, ICON_REMOVE,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        content.getChildren().add(imageContainer);
        
        
        videoContainer = new HBox();
        videoContainer.getChildren().add(new MediaView(new MediaPlayer(new Media("http://youtu.be/OHXjxWaQs9o"))));
        editVideoButton = initChildButton(videoContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeVideoButton = initChildButton(videoContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        content.getChildren().add(videoContainer);
        
        slideShowContainer = new HBox();
        editSlideShowButton = initChildButton(slideShowContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removeSlideShowButton = initChildButton(slideShowContainer, ICON_EDIT,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        content.getChildren().add(slideShowContainer);
    }
    
    private void initEventHandlers(){
        componentController = new ComponentController(ui);
        
        
        
        editHeaderButton.setOnAction(e -> {
            componentController.handleEditRequest("header",headerContainer);
        });
        
        editListButton.setOnAction(e -> {
            componentController.handleEditRequest("list",listContainer);
        });
        
        editImageButton.setOnAction(e -> {
            componentController.handleEditRequest("image",imageContainer);
        });
        
        editVideoButton.setOnAction(e -> {
            componentController.handleEditRequest("video",videoContainer);
        });
        
        editSlideShowButton.setOnAction(e -> {
            componentController.handleEditRequest("slideShow",slideShowContainer);
        });
        
        editParagraphButton.setOnAction(e -> {
            componentController.handleEditRequest("paragraph",paragraphContainer);
        });
        
        
        removeHeaderButton.setOnAction(e -> {
            componentController.handleRemoveRequest("header",headerContainer);
        });
        
        removeListButton.setOnAction(e -> {
            componentController.handleRemoveRequest("list",listContainer);
        });
        
        removeImageButton.setOnAction(e -> {
            componentController.handleRemoveRequest("image",imageContainer);
        });
        
        removeVideoButton.setOnAction(e -> {
            componentController.handleRemoveRequest("video",videoContainer);
        });
        
        removeSlideShowButton.setOnAction(e -> {
            componentController.handleRemoveRequest("slideShow",slideShowContainer);
        });
        
        removeParagraphButton.setOnAction(e -> {
            componentController.handleRemoveRequest("paragraph",paragraphContainer);
        });
        
    }
    
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
    
    public VBox getContentArea(){
        return contentArea;
    }
    
    public WebView createWebview() throws MalformedURLException{
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        html = Paths.get("./html/travel.html");
        File htmlSource = new File(html.toString());
        webEngine.load(htmlSource.toURI().toURL().toString());
        return browser;
    }
    
    //Toolabr for switching views
    private void initswitchViewToolbar() {
        switchViewToolbarPane = new FlowPane();
        //switchViewToolbarPane.setId("flow_pane");
        switchViewToolbarContainer = new HBox();
        switchViewToolbarContainer.getChildren().add(switchViewToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        editViewButton = initChildButton(switchViewToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(switchViewToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    private void initAddComponentToolbar() {
        addComponentToolbarPane = new FlowPane();
        //addComponentToolbarPane.setId("flow_pane");
        addComponentToolbarContainer = new HBox();
        addComponentToolbarContainer.getChildren().add(addComponentToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editViewButton = initChildButton(addComponentToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        liveViewButton = initChildButton(addComponentToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
}
