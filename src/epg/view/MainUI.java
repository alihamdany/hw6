package epg.view;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static epg.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.EDIT_VIEW;
import static epg.StartupConstants.ICON_EDIT;
import static epg.StartupConstants.ICON_EXIT;
import static epg.StartupConstants.ICON_LOAD_EPORTFOLIO;
import static epg.StartupConstants.ICON_NEW_EPORTFOLIO;
import static epg.StartupConstants.ICON_SAVEAS_EPORTFOLIO;
import static epg.StartupConstants.ICON_SAVE_EPORTFOLIO;
import static epg.StartupConstants.ICON_EXPORT_EPORTFOLIO;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.ICON_VIEW;
import static epg.StartupConstants.LIVE_VIEW;

import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_EXIT;
import static epg.StartupConstants.TOOLTIP_EXPORT_EPORTFOLIO;
import static epg.StartupConstants.TOOLTIP_LOAD_EPORTFOLIO;
import static epg.StartupConstants.TOOLTIP_NEW_EPORTFOLIO;
import static epg.StartupConstants.TOOLTIP_SAVEAS_EPORTFOLIO;
import static epg.StartupConstants.TOOLTIP_SAVE_EPORTFOLIO;
import epg.controller.ComponentController;
import epg.controller.FileController;
import epg.controller.PageEditController;

import epg.error.ErrorHandler;
import epg.file.FileManager;
import java.awt.Rectangle;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Priority;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading,
 * saving, editing, and viewing slide shows.
 *
 * @author McKilla Gorilla & _____________
 */
public class MainUI {
    
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    
    //UI
    public BorderPane userInterface;
    // WORKSPACE
    HBox workspace;
    
    //File Toolbar
    HBox fileToolbarContainer;
    FlowPane fileToolbarPane;
    
    //COntroller
    FileController fileController;
    FileManager fileManager;
    PageEditController pageEditController;
    
    //FILE TOOLBAR BUTTONS
    Button newEportfolioButton;
    Button loadEportfolioButton;
    Button saveEportfolioButton;
    Button saveAsEportfolioButton;
    Button exportEportfolioButton;
    Button exitButton;
    
    //Tab panes
    TabPane tabPane;
    
    //ADD Remove page toolbar
    VBox pageToolbarContainer;
    FlowPane pageToolbarPane;
    
    //ADD Remove page toolbar buttons
    Button addPageButton;
    Button removePageButton;
    
    
    
    
    
    
    
    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public MainUI(FileManager initFileManager) {
        
    }
    
    public void startUI(Stage initPrimaryStage, String windowTitle) throws MalformedURLException {
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();
        initPageToolbar();
        initTabPane();
        initWorkspace();
        
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
        initEventHandlers();
    }
    
    private void initWorkspace() {
        // FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
        workspace = new HBox();
        initPageToolbar();
        workspace.getChildren().add(pageToolbarContainer);
    }
    
    //File toolbar
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        //fileToolbarPane.setId("flow_pane");
        fileToolbarContainer = new HBox();
        fileToolbarContainer.getChildren().add(fileToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        newEportfolioButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO,	  TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadEportfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO,	  TOOLTIP_LOAD_EPORTFOLIO,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveEportfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO,	  TOOLTIP_SAVE_EPORTFOLIO,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveAsEportfolioButton = initChildButton(fileToolbarPane, ICON_SAVEAS_EPORTFOLIO, TOOLTIP_SAVEAS_EPORTFOLIO,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportEportfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT_EPORTFOLIO,	  TOOLTIP_EXPORT_EPORTFOLIO,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    public void initPageToolbar() {
        pageToolbarPane = new FlowPane(Orientation.VERTICAL);
        pageToolbarPane.setColumnHalignment(HPos.LEFT); // align labels on left
        pageToolbarPane.setPrefWrapLength(100);
        pageToolbarPane.setId("flow_pane");
        pageToolbarContainer = new VBox();
        pageToolbarContainer.getChildren().add(pageToolbarPane);
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        addPageButton = initChildButton(pageToolbarPane, ICON_EDIT,	  EDIT_VIEW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removePageButton = initChildButton(pageToolbarPane, ICON_VIEW,	  LIVE_VIEW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);
        
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        
        userInterface = new BorderPane();
        
        userInterface.setTop(fileToolbarContainer);
        //userInterface.getChildren().add(switchViewToolbarContainer);
        //userInterface.getChildren().add(pageToolbarContainer);
        //userInterface.getChildren().add(addComponentToolbarContainer);
        primaryScene = new Scene(userInterface);
        
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        String cssPath = STYLE_SHEET_UI;
        File css = new File(cssPath);
        
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    public void updateToolbarControls(boolean saved){
        
        userInterface.setCenter(workspace);
        saveEportfolioButton.setDisable(saved);
        exportEportfolioButton.setDisable(false);
    }
    
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip.toString());
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        //toolbar.setId("vertical_toolbar");
        return button;
    }
    
    public void initTabPane(){
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
        tabPane.setId("tabpane");
        
        HBox.setHgrow(tabPane, Priority.ALWAYS);
    }
    
    
    
    public void addPage() throws MalformedURLException{
        
        PageUI page = new PageUI(this);
        Tab tab1 = new Tab("PAGE 1");
        tab1.setContent(page.getContentArea());
        tabPane.getTabs().add(tab1);
    }
    
    private void initEventHandlers(){
        fileController = new FileController(this, fileManager);
        
        pageEditController = new PageEditController(this);
        newEportfolioButton.setOnAction(e -> {
            fileController.handleNewEportfolioRequest();
            workspace.getChildren().add(tabPane);
        });
        
        addPageButton.setOnAction(e -> {
            try {
                pageEditController.handleAddPageRequest();
            } catch (MalformedURLException ex) {
                Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
 }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    