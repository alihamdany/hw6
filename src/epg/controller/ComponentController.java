/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.view.HeaderEditView;
import epg.view.ImageEditView;
import epg.view.ListEditView;
import epg.view.MainUI;
import epg.view.ParagraphEditView;
import epg.view.SlideshowEditView;
import epg.view.VideoEditView;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

/**
 *
 * @author Ali
 */
public class ComponentController {
    private MainUI ui;
    
    public ComponentController(MainUI theui){
        
        ui = theui;
}
    
    public void handleEditRequest(String component, HBox container){
            
        if(component.equals("header"))
            new HeaderEditView();
        else if(component.equals("paragraph"))
            new ParagraphEditView();
        else if(component.equals("list"))
            new ListEditView();
        else if(component.equals("image"))
            new ImageEditView();
        else if(component.equals("video"))
            new VideoEditView();
        else if(component.equals("slideShow"))
                new SlideshowEditView();
        else{}
        
        
        
        }
    
    public void handleRemoveRequest(String component, HBox container){
        
        container.getChildren().clear();
        
    }
    
}
